---
title: An experiment in document construction
subtitle: Using XeLaTeX and Pandoc
author: Mark Ayers

papersize: letter
fontsize: 12pt
documentclass: scrartcl
geometry: margin=1.5in
mainfont: "Go-Regular.ttf"
mainfontoptions: "Path=./fonts/"
monofont: "Go-Mono.ttf"
monofontoptions: "Path=./fonts/"

keywords:
  - test
  - experiment
  - pdf
  - pandoc
  - latex
abstract: |
  Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
  quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
  consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
  cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
  proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
...

Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium
doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore
veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim
ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia
consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.

```go
package main

import fmt

func main() {
    fmt.Println("Hello")
}
```

Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur,
adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et
dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis
nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex
ea commodi consequatur?

---

© 2017 by Mark Ayers. Built with XeLaTeX and Pandoc
