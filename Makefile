.DEFAULT_GOAL := help
.PHONY: help
help:
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

sources := $(wildcard *.md)
targets := $(sources:%.md=%.pdf)

.PHONY: all
all: $(targets)  ## Generate PDF files from Markdown files

%.pdf: %.md
	pandoc --smart --normalize --standalone \
		--from=markdown+yaml_metadata_block \
		--to latex --latex-engine=xelatex \
		--output "$@" "$<"

.PHONY: pre-commit
pre-commit: ## Lint markdown source files
	mdl $(sources)
	yamllint $(wildcard *.yml)

.PHONY: clean
clean: ## Remove generated PDF files
	-rm -rf $(targets)
